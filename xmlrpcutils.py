import xmlrpc.client
import os
import datetime


class xmlrpc_connection(object):
    is_connect = False
    def __init__(self):

        self.username = ''
        self.pwd = ''
        self.dbname = ''
        self.host = ''
        self.port = ''


    def connect(self):
        sock_common = xmlrpc.client.ServerProxy(
            'http://%s:%s/xmlrpc/common' % (self.host, self.port))
        self.uid = sock_common.login(self.dbname, self.username, self.pwd)
        self.sock = xmlrpc.client.ServerProxy(
            'http://%s:%s/xmlrpc/object' % (self.host, self.port))
        self.is_connect = True

    def disconnect(self):
        self.is_connect = False
        self.sock = None

    def execute(self, model, method, *args):
        return self.sock.execute(self.dbname, self.uid, self.pwd, model, method, *args)

    def exec_workflow(self, model, method, record_id):
        return self.sock.exec_workflow(self.dbname, self.uid, self.pwd, model, method, record_id)
