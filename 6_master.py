from xmlrpcutils import xmlrpc_connection

odoo = xmlrpc_connection()
odoo.connect()

offset=0
limit=10
order="id desc"
fields = []

# product.product
# => shorturl.at/ipJSV
ids = odoo.execute('product.product','search',[[]],offset,limit,order)
data = odoo.execute('product.product','read',ids,fields)

# stock.location
# list of stock.location
# => shorturl.at/rsMW1
ids = odoo.execute('stock.location','search',[[]],offset,limit,order)
data = odoo.execute('stock.location','read',ids,fields)


# stock.warehouse
# => shorturl.at/zPUZ1
ids = odoo.execute('stock.warehouse','search',[[]],offset,limit,order)
data = odoo.execute('stock.warehouse','read',ids,fields)

# stock.journal ( stock.move mapping default)
# list of stock.journal
# => shorturl.at/hvKST 
ids = odoo.execute('stock.journal','search',[[]],offset,limit,order)
data = odoo.execute('stock.journal','read',ids,fields)