
from xmlrpcutils import xmlrpc_connection

odoo = xmlrpc_connection()
odoo.connect()

# def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):

offset=0
limit=10
order="id desc"

# search PO
po_ids = odoo.execute('purchase.order','search',[['state','=','approved']],offset,limit,order)
print(po_ids)

# read PO data
fields=[] # all fields
po_data = odoo.execute('purchase.order','read',po_ids,fields)
print(po_data)