
from xmlrpcutils import xmlrpc_connection

odoo = xmlrpc_connection()
odoo.connect()

# get stock.production.lot id from barcode
prodlot_id = False  # < assign id

# read data from stock.production.lot
prodlot = odoo.execute('stock.production.lot','read', prodlot_id)
# get product_id from stock.production.lot
product_id = prodlot['product_id'][0] # 0  = id , 1 = name (relation field)
# get product's data from product.product
product = odoo.execute('product.product','read',product_id)

# create stock.picking
stock_journal_id = 110
# list of stock.journal
# => shorturl.at/hvKST 

# list of stock.location
# => shorturl.at/rsMW1

# from location
location_id = 0
# to location
location_dest_id = 0

move_lines = [(0,0,{ # line 1
                'name':'description',
                'product_id':product_id,
                'prodlot_id':prodlot_id,
                'location_id': location_id,
                'location_dest_id':location_dest_id,
                'product_qty': 100,
                'product_uom': product['uom_id'][0]
            }),
            (0,0,{ # line 2 if continue
                'name':'description',
                'product_id':product_id, 
                'prodlot_id':prodlot_id,
                'location_id': location_id,
                'location_dest_id':location_dest_id,
                'product_qty': 100,
                'product_uom': product['uom_id'][0]
            }),

]

create_vals = {
    'stock_journal_id': stock_journal_id,
    'move_lines':move_lines
}

# create draft order
stock_picking_id = odoo.execute('stock.picking','create',create_vals)
# validate stock move from draft
odoo.execute('stock.picking','draft_validate',[stock_picking_id]) 