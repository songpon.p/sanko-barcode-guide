
from xmlrpcutils import xmlrpc_connection

odoo = xmlrpc_connection()
odoo.connect()

# ref 4_so.py


# find picking from SO
sale_order_id=

# search picking
picking_ids = odoo.execute('stock.picking','search',[['sale_id','=',sale_order_id]])

# Example 1 Validate by line
# search stock.move 
moves = odoo.execute('stock.move','search',[['picking_id','in',picking_ids]])

# just loop for example
for move in moves:
    move_id = move['id']
    # check stock balance
    odoo.execute('stock.move','action_assign',[move_id])
    # validate
    odoo.execute('stock.move','action_done',[move_id])

# Example 2 validate all item

# check avaiable
odoo.execute('stock.picking','action_assign',picking_ids)
# validate
odoo.execute('stock.picking','action_move',picking_ids)
